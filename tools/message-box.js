'use strict';
class MessageBox extends React.Component {
    constructor(props) {
        super(props);
        this.style = {
            containerStyle: {
                position: "fixed",
                top: "0",
                width: "100%",
                height: "100vh",
                backgroundColor: "rgba(255,235,250,0.4)",
                zIndex: "999",
            },
            insideContainerStyle:{
                width: "100%",
                height: "100vh",
                display:"grid"
            },
            insideContentStyle:{
                margin:"auto",
                width: "70%",
                backgroundColor: "white",
                boxShadow:"0px 0px 10px #707070",
                borderRadius:'10px',
            },
            headerStyle:{
                width:"100%",
                backgroundColor:"#CE4329",
                borderTopLeftRadius:"10px",
                borderTopRightRadius:"10px",
                color:"white",
            },
            headerInsideStyle:{
                padding:"10px",
                display: "flex",
                justifyContent:"space-between"
            },
            messageInsideStyle:{
                padding:"30px",
            },
            headerCloseStyle:{
                cursor :"pointer",
            }
        }
    }

    static call(DOM,target) {
        if(target!==undefined){
            XedScroll.goto(target.offsetTop-150);
            // window.scrollTo(0,target.offsetTop-150);
        }
        const domContainer = document.querySelector('#message-box');
        ReactDOM.render(DOM, domContainer);
    }
    close(){
        if(this.props.callback !== undefined){
            this.props.callback();
        }
        const domContainer = document.querySelector('#message-box');
        ReactDOM.render(<div></div>, domContainer);
    }
    render() {
        return (<div className={"message-box container"} style={this.style.containerStyle}>
            <div className={"message-box inside-container"} id={"closing-container"}
                 onClickCapture={(e)=>{
                     if(e.target.id === "closing-container")  {
                         this.close();
                     }
                 }}
                 style={this.style.insideContainerStyle}>
                <div className={"message-box inside-content"} style={this.style.insideContentStyle}>
                    <div className={"message-box header"} style={this.style.headerStyle}>
                        <div className={"message-box header-inside"} style={this.style.headerInsideStyle}>
                            <div className={"message-box header-text"}><h2 style={{margin:"auto"}}>{this.props.header}</h2></div>
                            <div className={"message-box header-close"} style={this.style.headerCloseStyle} onClickCapture={()=>{
                                this.close();
                            }}>
                                <h4><i className="material-icons">
                                    close
                                </i></h4>
                            </div>
                        </div>
                    </div>
                    <div className={"message-box message"}>
                        <div className={"message-box message-inside"} style={this.style.messageInsideStyle}>
                           <h3 style={{margin:"auto",textAlign:"center"}}>{this.props.message}</h3>
                        </div>
                    </div>
                    {/*<div className={"message-box confirm"}>*/}
                    {/*    <div className={"message-box confirm-inside"}>*/}
                    {/*        <a className={"button"}>Devam Et</a>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </div>
            </div>
        </div>)
    }
}