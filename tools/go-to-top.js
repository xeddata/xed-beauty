class GoToTop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            display:"none",};
        this.style = {
            inside: {
                borderRadius: "10px",
                backgroundColor: "rgba(10, 80, 82, 0.5)",
                width: "75px",
                height: "75px",
                color: "white",
            }
        };
        this.state = {display:"none"};
    }
    componentDidMount(){
        document.addEventListener("scroll",this.scrollHandler.bind(this));
    }
    componentWillUnmount(){
        document.removeEventListener("scroll",this.scrollHandler.bind(this));
    }

    scrollHandler(){
        // console.log(document.documentElement.scrollTop);

        if(document.documentElement.scrollTop >= 200){
           this.setState({display:"grid"});
        }else{
            this.setState({display:"none"});
        }
    }
    render() {
        return (
            <div style={{  display:this.state.display,
                position: "fixed",
                bottom: "20px",
                right: "20px",
                width: "75px",
                height: "75px",}}>
                <button style={this.style.inside} onClickCapture={()=>{
                    // window.scrollTo(0,0);
                    XedScroll.goto(0);
                }}><span className="material-icons">
                arrow_upward
                </span>
                    <br/>
                    <span>En Tepeye Dön</span>
                </button>
            </div>
        );
    }

}

const domContainer = document.querySelector('#go-to-top');
ReactDOM.render(<GoToTop/>, domContainer);