'use strict';
class Button extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {

    }
    componentWillUnmount(){
    }
    render() {
        let optional = [];
        if(this.props.optional !== null || this.props.optional !== undefined){
            optional = [<div className={'input-sub-info'}><h4>{this.props.optional}</h4></div>]
        }
        return(<div className={this.props.classes + ' container'}>
            <button id={this.props.buttonId} className={this.props.classes}><h3>{this.props.text}</h3></button>
            {optional}
        </div>);
    }
}