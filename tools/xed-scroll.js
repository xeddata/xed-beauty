class XedScroll {
    static goto(y) {
        if(window.scrollAnimation || window.scrollAnimation === undefined){
            window.scrollAnimation = false;
            let timer = setInterval(() => {
                let dif = (-1)*(window.scrollY -y)/10;
                dif = Math.floor(dif);
                window.scrollTo(0, window.scrollY+dif);
                // console.log(dif,y,window.scrollY);
                if(dif === 0 || window.scrollY === 0){
                    window.scrollAnimation = true;
                    clearInterval(timer);
                }
            }, 10);
        }
    }
}