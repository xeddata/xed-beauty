'use strict';

class TextInput extends React.Component {
    constructor(props) {
        super(props);
        this.inputStyle = {
            borderRadius: "10px",
            border: "1px solid #707070",
            backgroundColor: "rgba(133, 133, 133, 0)",
            height: this.props.height + "px",
            fontSize: this.props.height / 2 + "px",
            padding: this.props.height / 5 + "px",
            width: "-webkit-fill-available",
            transition: "0.3s",
            outlineColor: "#737373",
        };
        this.inputFocus = {
            borderRadius: "10px",
            border: "2px solid #707070",
            backgroundColor: "rgba(133, 133, 133, 0)",
            height: this.props.height + "px",
            fontSize: this.props.height / 2 + "px",
            padding: this.props.height / 5 + "px",
            width: "-webkit-fill-available",
            transition: "0.3s",
            outline: "none",
        };
        this.placeholderFocus = {
            color: "#8c8c8c",
            paddingLeft: this.props.height / 5 + "px",
            paddingRight: this.props.height / 5 + "px",
            backgroundColor: this.props.backgroundColor || "white",
            fontSize: this.props.height / 3 + "px",
            position: "relative",
            left: this.props.height / 2 + "px",
            transition: "0.3s",
            width: "fit-content",
            top: "-" + (this.props.height + this.props.height- this.props.height/5) + "px",
            zIndex: "1",

        };
        this.placeholderStyle = {
            color: "#8c8c8c",
            zIndex: "-1",
            paddingLeft: this.props.height / 5 + "px",
            paddingRight: this.props.height / 5 + "px",
            backgroundColor: this.props.backgroundColor || "white",
            fontSize: this.props.height / 2.5 + "px",
            position: "relative",
            left: this.props.height / 10 + "px",
            top: "-" + (this.props.height) + "px",
            transition: "0.3s",
            width: "fit-content",
        };
        this.state = {
            cursorPosition: 0,
            type: this.props.type || "text",
            color: "#737373",
            format: this.props.format,
            value: "",
            readValue: "",
            validText: "",
            isValid: false,
            hasValue: '',
            formatCounter: 0,
            inputStyle: this.inputStyle,
            validationStyle: {
                zIndex: "-1",
                backgroundColor: "rgba(0,0,0,0)",
                fontSize: this.props.height / 2 + "px",
                position: "relative",
                left: "-10px",
                top: "-" + (this.props.height) + "px",
                transition: "0.3s",
                width: "100%",
            },
            placeholderStyle: this.placeholderStyle
        }
    }

    componentWillMount() {
        this.formatCounter();
    }

    componentDidMount() {
        document.getElementById(this.props.inputId).addEventListener('focusout', this.focusOutHandler.bind(this));
        document.getElementById(this.props.inputId).addEventListener('change', this.focusOutHandler.bind(this));
        document.getElementById(this.props.inputId).addEventListener('focus', this.focusHandler.bind(this));

    }

    componentWillUnmount() {
        document.getElementById(this.props.inputId).removeEventListener('focusout', this.focusOutHandler.bind(this));
        document.getElementById(this.props.inputId).removeEventListener('change', this.focusOutHandler.bind(this));
        document.getElementById(this.props.inputId).removeEventListener('focus', this.focusHandler.bind(this));
    }

    focusHandler() {
        this.validation();
        let style = this.placeholderFocus;
        this.setState({placeholderStyle: style});
        this.setState({inputStyle: this.inputFocus});
        if (this.props.format !== undefined) {
            this.setState({value: this.formatter(this.state.readValue)});
            this.validation();
        }
    }

    focusOutHandler() {
        this.validation();
        this.setState({inputStyle: this.inputStyle});
        if (document.querySelector('#' + this.props.inputId).value === "") {
            this.setState({hasValue: ''});
            let style = this.placeholderStyle;
            this.setState({placeholderStyle: style});

        } else {
            this.setState({hasValue: 'has-value'});
        }

        if (this.props.format !== undefined && this.state.readValue.length === 0) {
            this.setState({hasValue: ''});
            let style = this.placeholderStyle;
            this.setState({placeholderStyle: style});
            this.setState({value: ""});
        }
    }

    formatCounter() {
        let count = 0;
        if (this.state.format !== undefined) {
            for (let i = 0; i < this.state.format.length; i++) {
                if (this.state.format[i] === "n") {
                    count++;
                }
            }
            this.setState({formatCounter: count});
        }
    }

    formatter(readValue) {
        let val = "";
        let cn = 0;
        let selectPrint = false;
        for (let i = 0; i < this.state.format.length; i++) {
            if (this.state.format[i] === "n") {
                if (readValue.length > cn) {
                    val += readValue[cn];
                    cn++;
                } else {
                    if (!selectPrint) {
                        this.setState({cursorPosition: i});
                        // console.log(this.state.cursorPosition);
                        selectPrint = true;
                    }
                    if(this.props.mask){
                        val += "_"
                    }
                }
            } else {
                if (this.state.format[i] !== ".") {
                    val += this.state.format[i];
                } else {
                    if (cn === this.state.formatCounter && this.state.readValue.length !== 0) {
                        this.setState({cursorPosition: i});
                    }
                }
            }
        }
        return val;
    }

    readValueHandler(key) {
        let val = "";
        if (key.match(/\d{1}/g) && this.state.readValue.length < this.state.formatCounter) {
            val = this.state.readValue + key;
            this.setState({readValue: this.state.readValue + key});
            return this.formatter(val);
        } else if (key === "Backspace") {
            val = this.state.readValue.substring(0, this.state.readValue.length - 1);
            this.setState({readValue: this.state.readValue.substring(0, this.state.readValue.length - 1)});
            return this.formatter(val);
        } else {
            return this.state.value;
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.value !== this.state.value) {
            this.validation();
        }
    }

    // componentWillUpdate(nextProps,nextState){
    //     if (nextState.value !== this.state.value) {
    //     }
    // }

    validation() {
        if (this.state.value !== "") {
            if (this.props.validation(this.state.value)) {
                this.setState({validText: "done"});
                this.setState({color: "#0A5052"});
            } else {
                this.setState({validText: "close"});
                this.setState({color: "#CE4329"});
            }
        } else {
            if (this.props.optional === undefined) {
                this.setState({validText: "close"});
                this.setState({color: "#CE4329"});
            } else {
                this.setState({validText: ""});
            }

        }
    }

    render() {

        let optional = [];
        if (this.props.optional !== undefined) {
            optional = [<div className={'input-sub-info'}><h4 style={{float: "right"}}>{this.props.optional}</h4></div>]
        }
        let placeholder = this.props.placeholder;
        if(this.props.optional === undefined){
            placeholder = placeholder+"*";
        }
        // this.validation();
        return (<div className={this.props.classes + ' container'}
                     style={{width: "100%", display: "grid", transition: "0.3s"}}>
            <div>
            {/*<span className={this.props.classes + ' placeholder'}>{this.props.placeholder}</span>*/}
            {/*{optional}*/}
            <input type={this.state.type} id={this.props.inputId} className={this.props.classes}
                   style={this.state.inputStyle} onChange={e => {
                if (this.props.format === undefined) {
                    this.setState({value: e.target.value});
                    this.validation();
                }
            }}
                   value={this.state.value}
                   onKeyDownCapture={(e) => {
                       if (this.props.format !== undefined) {
                           e.preventDefault();
                           this.setState(
                               {value: this.readValueHandler(e.key)},
                               () => {
                                   event.target.selectionStart = event.target.selectionEnd = this.state.cursorPosition;
                               });
                       }
                       this.validation();
                   }}
                   onFocusCapture={(e) => {
                       if (this.props.format !== undefined) {
                           e.preventDefault();
                           this.setState(
                               {value: this.readValueHandler("")},
                               () => {
                                   event.target.selectionStart = event.target.selectionEnd = this.state.cursorPosition;
                               });
                       }
                   }}
            />
            <label for={this.props.inputId} className={this.props.classes + ' placeholder ' + this.state.hasValue}
                   style={this.state.placeholderStyle}>{placeholder}</label>
            <span style={this.state.validationStyle}><i className="material-icons" style={{
                fontSize: this.props.height * 3 / 5 + "px",
                float: "right",
                color: this.state.color
            }}>
            {this.state.validText}
            </i></span>
            </div></div>);
    }
}